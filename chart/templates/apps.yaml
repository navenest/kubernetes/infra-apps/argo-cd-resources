apiVersion: argoproj.io/v1alpha1
kind: AppProject
metadata:
  name: apps
  # annotations:
  #   notifications.argoproj.io/subscribe.on-sync-succeeded.slack: releases
  # Finalizer that ensures that project is not deleted until it is not referenced by any application
  finalizers:
    - resources-finalizer.argocd.argoproj.io
spec:
  # Project description
  description: Apps Project

  # Allow manifests to deploy from any Git repos
  sourceRepos:
  - '*'

   # Only permit applications to deploy to the argocd namespace in the same cluster
  destinations:
  - server: https://kubernetes.default.svc
    namespace: '*'

  clusterResourceWhitelist:
  - group: '*'
    kind: '*'
---
apiVersion: argoproj.io/v1alpha1
kind: ApplicationSet
metadata:
  name: apps
spec:
  generators:
    - matrix:
        generators:
          - scmProvider:
              cloneProtocol: https
              gitlab:
                group: "navenest/kubernetes/apps"
                api: "https://gitlab.com/"
                allBranches: false
                includeSubgroups: false
                tokenRef:
                  secretName: gitlab-creds
                  key: password
              filters:
                - pathsExist:
                    - clusters/{{.Values.cluster}}/config.yaml
          - git:
              repoURL: {{`'{{ url }}'`}}
              revision: HEAD
              files:
                - path: "clusters/{{.Values.cluster}}/config.yaml"

  syncPolicy:
    preserveResourcesOnDeletion: true

  template:
    metadata:
      name: {{`'{{ name }}'`}}
      annotations:
        argocd.argoproj.io/manifest-generate-paths: "clusters/{{.Values.cluster}}"
    spec:
      project: apps
      source:
        path: "clusters/{{.Values.cluster}}"
        targetRevision: HEAD
        repoURL: {{`'{{ url }}'`}}
        helm:
          valueFiles:
            - ../../common/values.yaml
            - {{`'{{ valueFile }}'`}}
          version: v3
      destination:
        server: https://kubernetes.default.svc
        namespace: {{`'{{ namespace }}'`}}
        
      syncPolicy:
        automated: # automated sync by default retries failed attempts 5 times with following delays between attempts ( 5s, 10s, 20s, 40s, 80s ); retry controlled using `retry` field.
          prune: true # Specifies if resources should be pruned during auto-syncing ( false by default ).
          selfHeal: true # Specifies if partial app sync should be executed when resources are changed only in target Kubernetes cluster and no git change detected ( false by default ).
          allowEmpty: false # Allows deleting all application resources during automatic syncing ( false by default ).
        syncOptions:     # Sync options which modifies sync behavior
        - Validate=true # disables resource validation (equivalent to 'kubectl apply --validate=false') ( true by default ).
        - CreateNamespace=true # Namespace Auto-Creation ensures that namespace specified as the application destination exists in the destination cluster.
        - PrunePropagationPolicy=foreground # Supported policies are background, foreground and orphan.
        - PruneLast=true # Allow the ability for resource pruning to happen as a final, implicit wave of a sync operation
        - ServerSideApply={{`{{ ServerSideApply }}`}}
        # The retry feature is available since v1.7
        retry:
          limit: 5 # number of failed sync attempt retries; unlimited number of attempts if less than 0
          backoff:
            duration: 5s # the amount to back off. Default unit is seconds, but could also be a duration (e.g. "2m", "1h")
            factor: 2 # a factor to multiply the base duration after each failed retry
            maxDuration: 3m # the maximum amount of time allowed for the backoff strategy
      ignoreDifferences:
        - group: monitoring.coreos.com
          kind: ServiceMonitor
          jqPathExpressions:
          - .spec.endpoints[]?.relabelings[]?.action
